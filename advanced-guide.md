# Advanced Guide

## 들어가기 전에

1. 해당 가이드는 GGM이 가이드하는 기본 가이드([JS](./js-guide.md),[TS](./ts-guide.md))를 전부 숙지했다는 전제 하에 작성된다.
2. 실 개발에서 마주칠만한 문제점에 대해 생각해볼 수 있도록 가이드가 구성된다.

## 목표

1. 실 개발 환경에서 접하게 될 수 있는 여러 문제점에 대해 이해하고 해결책을 숙지한다.

## 가이드

[Ref와 DOM](https://ko.reactjs.org/docs/refs-and-the-dom.html)

[[React] Custom-hook 공식문서 번역](https://velog.io/@adam2/-React-Custom-hook-%EA%B3%B5%EC%8B%9D%EB%AC%B8%EC%84%9C-%EB%B2%88%EC%97%AD)

[[번역] useEffect 완벽 가이드](https://rinae.dev/posts/a-complete-guide-to-useeffect-ko)

에러 처리를 어떻게 하면 좋을까? [1](https://rinae.dev/posts/how-to-handle-errors-1), [2](https://rinae.dev/posts/how-to-handle-errors-2), [3](https://rinae.dev/posts/how-to-handle-errors-3)

[[번역] 자바스크립트 & 타입스크립트의 순환 참조를 한방에 해결하는 방법](https://rinae.dev/posts/fix-circular-dependency-kr)


### Unit Test

[[번역] 리액트 테스팅 튜토리얼: 테스트 프레임워크와 컴포넌트 테스트 방법](https://rinae.dev/posts/react-testing-tutorial-kr)

[[번역] 리액트 + 리덕스 앱을 Jest와 Enzyme으로 테스트 하며 얻은 교훈](https://rinae.dev/posts/lessons-learned-testing-react-redux-apps-with-jest-and-enzyme-kr)