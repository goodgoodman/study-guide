# TS Guide


## 들어가기 전에

1. 해당 가이드는 [JS Guide](./js-guide.md)를 숙지한 상황을 전제로 작성됨.
2. CRA 없이 직접 프로젝트를 구성할 수 있어야함.

## 목표

1. 기존의 JS대신 TS를 사용할 수 있다.
2. TypeScript의 Type과 그 활용방법, 그리고 js가 어떻게 바인딩 되는지 이해하고, 문제가 발생했을때 해결할 수 있다.

## 가이드

[TypesScript GitBook](https://typescript-kr.github.io/pages/Basic%20Types.html) <-- 5분 안에 보는 TypeScript 와 핸드북 카테고리, tsconfig.json정도만 간단하게 볼 것

[리액트 프로젝트에서 타입스크립트 사용하기](https://velog.io/@velopert/series/react-with-typescript)

[타입스크립트에서 헬퍼 타입을 정의하고 활용하기](https://rinae.dev/posts/helper-types-in-typescript)

[Utility Types](https://www.typescriptlang.org/docs/handbook/utility-types.html)

## Toy Project

- Nihon-Go 개발
