# JS Guide

## 들어가기 전에 

1. 기본적으로 VSCode를 사용, 필요시 다른 에디터를 사용해도 되나 설정은 통합해야함.
2. Typescript는 해당 가이드가 끝난 다음 별개의 가이드를 따름.
3. CSS는 완전 별개이므로 직접 공부하면서 찾아써야함. 해당 가이드에선 CSS를 다루지 않음.

## 목표 

1. 프로젝트를 자유 자재로 구성할 수 있다. (CRA 말고 직접 Manual하게 Parcel을 사용해서 프로젝트를 구성할 수 있어야 함)
2. 개발에 필요한 최소한의 지식(JS[ES Next] + React + Redux)과 추가적인 도구를 익힌다.
3. 코딩 스탠다드와 Eslint를 숙지한다.

> 해당 가이드는 순서대로 따라가는 것을 권장한다.


## 기초 가이드

[javascript-basics-for-absolute-beginners](https://academy.nomadcoders.co/p/javascript-basics-for-absolute-beginners-kr)

[gulp-crash-course](https://academy.nomadcoders.co/p/gulp-crash-course)

[reactjs-fundamentals](https://academy.nomadcoders.co/p/reactjs-fundamentals)

[learn-parcel](https://academy.nomadcoders.co/p/learn-parcel)

[styled-components-like-a-boss](https://academy.nomadcoders.co/p/styled-components-like-a-boss)

[mastering-react-16](https://academy.nomadcoders.co/p/mastering-react-16)

[antiredux-new-react-context-api](https://academy.nomadcoders.co/p/antiredux-new-react-context-api)

[build-a-timer-app-with-react-native-and-redux](https://academy.nomadcoders.co/p/build-a-timer-app-with-react-native-and-redux)

## 포스트

[리액트 프로젝트에 ESLint 와 Prettier 끼얹기](https://velog.io/@velopert/eslint-and-prettier-in-react)

[ESLint](https://poiemaweb.com/eslint)

[JavaScript Standard Style](https://standardjs.com/rules-kokr.html) <-- eslint 에서 설정해서 가져다 쓸 수도 있음

[ES6 array helper method들을 알아보자](https://gnujoow.github.io/dev/2016/10/14/Dev6-es6-array-helper/)

[ECMAScript](https://www.zerocho.com/category/ECMAScript?page=3) <-- ECMAScript 카테고리에 해당하는거 포스트 들을 읽고 아 ~한 기능이 있구나 정도만 알면됨

[누구든지 하는 리액트](https://velopert.com/reactjs-tutorials) <-- Basisc에 해당하는 내용들만 보면 됨

[Why react hooks?](https://dev-momo.tistory.com/entry/React-Hooks)

[예제로 따라하는 리액트 훅(hook)](https://codingbroker.tistory.com/30) <-- axios 사용부분 유심히 잘 봐야함

[Redux 또는 MobX 를 통한 상태 관리](https://velog.io/@velopert/series/redux-or-mobx) <-- MobX부터는 Optional

## optional

[React Hooks가 Redux를 대체할 수 있냐고 물어보지 마세요](https://delivan.dev/react/stop-asking-if-react-hooks-replace-redux-kr/)

[Material UI](https://material-ui.com/) <-- 한번 써볼것

[Redux FAQ: Code Structure](https://redux.js.org/faq/code-structure#what-should-my-file-structure-look-like-how-should-i-group-my-action-creators-and-reducers-in-my-project-where-should-my-selectors-go)

[vscode-styled-components](https://marketplace.visualstudio.com/items?itemName=jpoissonnier.vscode-styled-components)

## Toy project

- 자신의 웹 이력서 만들기(Skill, Project 등을 설명할 수 있는)

## 그 외 참고자료

[Redux GitBook](https://lunit.gitbook.io/redux-in-korean/)

[React Docs](https://ko.reactjs.org/docs/hello-world.html)